<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Espece extends Model
{
    protected $table = 'especes';

    protected $fillable = [
        'name', 'slug', 'type_vegetal_id'
    ];
}
