<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historique extends Model
{
    protected $table = 'historiques';

    protected $fillable = [
        'taille', 'arrose', 'engrais', 'vegetal_id'
    ];
}
