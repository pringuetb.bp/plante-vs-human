<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fiche extends Model
{
    protected $table = 'fiches';

    protected $fillable = [
        'exposition', 'type_sol', 'taille', 'arrosage_user', 'arrosage_compt', 'conseils', 'image', 'espece_id'
    ];
}
