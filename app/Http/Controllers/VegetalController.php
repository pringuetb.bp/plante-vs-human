<?php

namespace App\Http\Controllers;

use App\Vegetal;
use App\Historique;
use App\Http\Requests\VegetalRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VegetalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Ajout d'un végétal
     */
    public function store(VegetalRequest $request)
    {
        $params            = $request->validated();
        $params['user_id'] = Auth::user()->id;
        Vegetal::create($params);

        return back();
    }

    /**
     * Modification d'un végétal
     */
    public function update(VegetalRequest $request, int $id)
    {
        $params  = $request->validated();
        $vegetal = Vegetal::findOrFail($id);
        if ($vegetal->user_id == Auth::user()->id) {
            $vegetal->update($params);
        }

        return back();
    }

    /**
     * Suppression d'un végétal
     */
    public function remove(int $id)
    {
        $vegetal = Vegetal::findOrFail($id);
        if ($vegetal->user_id == Auth::user()->id) {
            $vegetal->delete();
        }

        return back();
    }
}
