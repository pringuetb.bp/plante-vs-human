<?php

namespace App\Http\Controllers;

use App\TypeVegetal;
use App\Http\Requests\TypeVegetalRequest;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TypeVegetalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Ajout d'un type de végétal
     */
    public function store(TypeVegetalRequest $request)
    {
        if (Auth::user()->role == 0) {
            $params         = $request->validated();
            $params['slug'] = Str::ascii($params['name']);
            TypeVegetal::create($params);
        }

        return back();
    }

    /**
     * Modification d'un type de végétal
     */
    public function update(TypeVegetalRequest $request, int $id)
    {
        if (Auth::user()->role == 0) {
            $params         = $request->validated();
            $params['slug'] = Str::ascii($params['name']);
            $typeVegetal    = TypeVegetal::findOrFail($id);
            $typeVegetal->update($params);
        }

        return back();
    }

    /**
     * Suppression d'un type de végétal
     */
    public function remove(int $id)
    {
        if (Auth::user()->role == 0) {
            $typeVegetal = TypeVegetal::findOrFail($id);
            $typeVegetal->delete();
        }

        return back();
    }
}
