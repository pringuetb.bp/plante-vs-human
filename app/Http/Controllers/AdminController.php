<?php

namespace App\Http\Controllers;

use App\Espece;
use App\TypeVegetal;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Page d'administration
     */
    public function index()
    {
        if (Auth::user()->role == 0) {
            $nbUsers      = sizeof(User::all());
            $typeVegetals = TypeVegetal::all();
            $especes      = DB::table('especes')
                ->select(['especes.*', 'type_vegetals.name AS type'])
                ->leftjoin('type_vegetals', 'especes.type_vegetal_id', '=', 'type_vegetals.id')
                ->orderBy('id', 'asc')
                ->get();
            $fiches       = DB::table('fiches')
                ->select(['fiches.*', 'especes.name', 'especes.slug', 'especes.type_vegetal_id', 'type_vegetals.name AS type'])
                ->join('especes', 'fiches.espece_id', '=', 'especes.id')
                ->join('type_vegetals', 'especes.type_vegetal_id', '=', 'type_vegetals.id')
                ->orderBy('updated_at', 'desc')
                ->get();
            $noFiche      = DB::select('SELECT id FROM especes WHERE id NOT IN (SELECT espece_id FROM fiches)');

            return view('admin.index')->with([
                'nbUsers'      => $nbUsers,
                'typeVegetals' => $typeVegetals,
                'especes'      => $especes,
                'fiches'       => $fiches,
                'noFiche'      => $noFiche
            ]);
        } else {
            return redirect()->route('dashbord');
        }
    }
}
