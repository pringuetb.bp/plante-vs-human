<?php

namespace App\Http\Controllers;

use App\Espece;
use App\Http\Requests\EspeceRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class EspeceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Ajout d'une espece
     */
    public function store(EspeceRequest $request)
    {
        if (Auth::user()->role == 0) {
            $params         = $request->validated();
            $params['slug'] = Str::ascii($params['name']);
            Espece::create($params);
        }

        return back();
    }

    /**
     * Modification d'une espece
     */
    public function update(EspeceRequest $request, int $id)
    {
        if (Auth::user()->role == 0) {
            $params         = $request->validated();
            $params['slug'] = Str::ascii($params['name']);
            $espece         = Espece::findOrFail($id);
            $espece->update($params);
        }

        return back();
    }

    /**
     * Suppression d'une espece
     */
    public function remove(int $id)
    {
        if (Auth::user()->role == 0) {
            $espece = Espece::findOrFail($id);
            $espece->delete();
        }

        return back();
    }
}
