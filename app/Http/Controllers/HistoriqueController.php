<?php

namespace App\Http\Controllers;

use Exception;
use App\Espece;
use App\Historique;
use App\Vegetal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\HistoriqueRequest;

class HistoriqueController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set('Europe/Paris');
        $this->middleware('auth');
    }

    /**
     * Affichage de l'historique d'une plante
     */
    public function index(int $id)
    {
        $user      = Auth::user()->id;
        $especes   = Espece::all();
        $vegetal   = DB::table('Vegetals')
            ->select(['vegetals.id', 'vegetals.name', 'vegetals.espece_id', 'especes.name AS espece', 'especes.slug AS especeSlug', 'type_vegetals.name AS type', 'vegetals.date_plantation'])
            ->join('especes', 'vegetals.espece_id', '=', 'especes.id')
            ->join('type_vegetals', 'especes.type_vegetal_id', '=', 'type_vegetals.id')
            ->where([
                ['vegetals.user_id', '=', $user],
                ['vegetals.id', '=', $id]
            ])
            ->get();
        try {
            $vegetal = $vegetal[0];
        } catch (Exception $e) {
            return back();
        }
        $vegetal->arrosee = DB::table('historiques')
            ->select(['date'])
            ->where([
                ['historiques.vegetal_id', '=', $vegetal->id],
                ['historiques.arrose', '=', '1']
            ])
            ->orderBy('date', 'DESC')
            ->limit(1)
            ->get();
        try {
            $vegetal->arrosee = $vegetal->arrosee[0]->date;
        } catch (Exception $e) {
            $vegetal->arrosee = null;
        }
        $vegetal->engrais = DB::table('historiques')
            ->select(['date'])
            ->where([
                ['historiques.vegetal_id', '=', $vegetal->id],
                ['historiques.engrais', '=', '1']
            ])
            ->orderBy('date', 'DESC')
            ->limit(1)
            ->get();
        try {
            $vegetal->engrais = $vegetal->engrais[0]->date;
        } catch (Exception $e) {
            $vegetal->engrais = null;
        }
        $vegetal->taille = DB::table('historiques')
            ->select(['taille'])
            ->where([
                ['historiques.vegetal_id', '=', $vegetal->id],
                ['historiques.taille', '<>', 'NULL']
            ])
            ->orderBy('date', 'DESC')
            ->limit(1)
            ->get();
        try {
            $vegetal->taille = $vegetal->taille[0]->taille;
        } catch (Exception $e) {
            $vegetal->taille = null;
        }
        $vegetal->fiche = DB::table('fiches')
                ->where('espece_id', '=', $vegetal->espece_id)
                ->get();
        try {
            $vegetal->fiche = $vegetal->fiche[0];
        } catch (Exception $e) {
            $vegetal->fiche = null;
        }
        $histories = DB::table('historiques')
            ->where('vegetal_id', '=', $vegetal->id)
            ->orderBy('historiques.date', 'DESC')
            ->get();

        $fiche     = DB::table('vegetals')
            ->select(['*'])
            ->join('especes', 'vegetals.espece_id', '=', 'especes.id')
            ->join('fiches', 'fiches.espece_id', '=', 'especes.id')
            ->where('user_id', '=', $user)
            ->where('vegetals.id', '=', $vegetal->id)
            ->get();
        $arose     = DB::table('vegetals')
            ->select(['*'])
            ->leftJoin('historiques', 'historiques.vegetal_id', '=', 'vegetals.id')
            ->where('vegetal_id', '=', $vegetal->id)
            ->where('arrose', '=', '1')
            ->orderBy('historiques.date', 'DESC')
            ->get();
        $day       = Carbon::now();
        $tailles   = DB::table('vegetals')
            ->select(['*'])
            ->leftJoin('historiques', 'historiques.vegetal_id', '=', 'vegetals.id')
            ->where('vegetal_id', '=', $vegetal->id)
            ->whereNull('historiques.arrose')
            ->where('historiques.engrais')
            ->orderBy('historiques.date', 'asc')
            ->get();

        return view('historique.index')->with([
            'vegetal'   => $vegetal,
            'especes'   => $especes,
            'histories' => $histories,
            'arose'     => $arose,
            'tailles'   => $tailles,
            'day'       => $day,
            'fiches'    => $fiche
        ]);
    }

    /**
     * Ajout d'eau à l'historique
     */
    public function storeWater(int $id)
    {
        $vegetal = Vegetal::findOrFail($id);
        if ($vegetal->user_id == Auth::user()->id) {
            DB::select('INSERT INTO historiques (`date`, arrose, vegetal_id) VALUES ("'.date('Y-m-d H:i:s').'", 1, '.$vegetal->id.')');
        }

        return back();
    }

    /**
     * Ajout d'engrais à l'historique
     */
    public function storeFertilizer(int $id)
    {
        $vegetal = Vegetal::findOrFail($id);
        if ($vegetal->user_id == Auth::user()->id) {
            DB::select('INSERT INTO historiques (`date`, engrais, vegetal_id) VALUES ("'.date('Y-m-d H:i:s').'", 1, '.$vegetal->id.')');
        }

        return back();
    }

    /**
     * Ajout d'une taille à l'historique
     */
    public function storeSize(HistoriqueRequest $request)
    {
        $params = $request->validated();
        $vegetal = Vegetal::findOrFail($params['vegetal_id']);
        if ($vegetal->user_id == Auth::user()->id) {
            DB::select('INSERT INTO historiques (`date`, taille, vegetal_id) VALUES ("'.date('Y-m-d H:i:s').'", '.$params['taille'].', '.$vegetal->id.')');
        }

        return back();
    }

    /**
     * Suppression d'un historique
     */
    public function remove(int $id)
    {
        $history = Historique::findOrFail($id);
        $vegetal = Vegetal::findOrFail($history->vegetal_id);
        if ($vegetal->user_id == Auth::user()->id) {
            $history->delete();
        }

        return back();
    }
}
