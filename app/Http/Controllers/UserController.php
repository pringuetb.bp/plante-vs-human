<?php

namespace App\Http\Controllers;

use Exception;
use App\User;
use App\Espece;
use App\Vegetal;
use App\Historique;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set('Europe/Paris');
        $this->middleware('auth');
    }

    /**
     * Affichage du tableau de bord de l'utilisateur
     */
    public function dashbord()
    {
        $user     = Auth::user()->id;
        $especes  = Espece::all();
        $vegetals = DB::table('Vegetals')
            ->select(['vegetals.id', 'vegetals.name', 'vegetals.espece_id', 'especes.name AS espece', 'especes.slug AS especeSlug', 'type_vegetals.name AS type', 'vegetals.date_plantation'])
            ->join('especes', 'vegetals.espece_id', '=', 'especes.id')
            ->join('type_vegetals', 'especes.type_vegetal_id', '=', 'type_vegetals.id')
            ->where('vegetals.user_id', '=', $user)
            ->get();
        foreach ($vegetals as $vegetal) {
            $vegetal->arrosee = DB::table('historiques')
                ->select(['date'])
                ->where([
                    ['historiques.vegetal_id', '=', $vegetal->id],
                    ['historiques.arrose', '=', '1']
                ])
                ->orderBy('date', 'DESC')
                ->limit(1)
                ->get();
            try {
                $vegetal->arrosee = $vegetal->arrosee[0]->date;
            } catch (Exception $e) {
                $vegetal->arrosee = null;
            }
            $vegetal->engrais = DB::table('historiques')
                ->select(['date'])
                ->where([
                    ['historiques.vegetal_id', '=', $vegetal->id],
                    ['historiques.engrais', '=', '1']
                ])
                ->orderBy('date', 'DESC')
                ->limit(1)
                ->get();
            try {
                $vegetal->engrais = $vegetal->engrais[0]->date;
            } catch (Exception $e) {
                $vegetal->engrais = null;
            }
            $vegetal->taille = DB::table('historiques')
                ->select(['taille'])
                ->where([
                    ['historiques.vegetal_id', '=', $vegetal->id],
                    ['historiques.taille', '<>', 'NULL']
                ])
                ->orderBy('date', 'DESC')
                ->limit(1)
                ->get();
            try {
                $vegetal->taille = $vegetal->taille[0]->taille;
            } catch (Exception $e) {
                $vegetal->taille = null;
            }
            $vegetal->fiche = DB::table('fiches')
                ->where('espece_id', '=', $vegetal->espece_id)
                ->get();
            try {
                $vegetal->fiche = $vegetal->fiche[0];
            } catch (Exception $e) {
                $vegetal->fiche = null;
            }
        }

        return view('user.dashbord')->with([
            'vegetals' => $vegetals,
            'especes'  => $especes,
        ]);
    }

    /**
     * Affichage les informations de l'utilisateur
     */
    public function profil()
    {
        return view('user.profil')->with([
        ]);
    }

    /**
     * Modification d'un utilisateur
     */
    public function update(UserRequest $request)
    {
        $user   = User::findOrFail(Auth::user()->id);
        $params = $request->validated();
        if (Hash::check($params['password'], $user->password)) {
            if ($params['new_password'] === $params['new_password_confirmation'] && !empty($params['new_password']) && !empty($params['new_password_confirmation'])) {
                $params['password'] = Hash::make($params['new_password']);
            } else {
                $params['password'] = $user->password;
            }
            $data = [
                'name'     => $params['name'],
                'email'    => $params['email'],
                'password' => $params['password'],
                'role'     => $user->role
            ];
            $user->update($data);
        }
        return back();
    }

    /**
     * Suppression d'un utilisateur
     */
    public function remove()
    {
        $user     = User::findOrFail(Auth::user()->id);
        $vegetals = DB::table('Vegetals')
            ->where('user_id', '=', $user->id)
            ->get();
        foreach ($vegetals as $vegetal) {
            $vegetal   = Vegetal::findOrFail($vegetal->id);
            $histories = DB::table('Historiques')
                ->where('vegetal_id', '=', $vegetal->id)
                ->get();
            foreach ($histories as $history) {
                $history = Historique::findOrFail($history->id);
                $history->delete();
            }
            $vegetal->delete();
        }
        $user->delete();

        return back();
    }
}
