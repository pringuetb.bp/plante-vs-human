<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Affichage de l'accueil du site
     */
    public function index()
    {
        $fiches = DB::table('fiches')
            ->select(['fiches.*', 'especes.name', 'especes.slug', 'especes.type_vegetal_id', 'type_vegetals.name AS type'])
            ->join('especes', 'fiches.espece_id', '=', 'especes.id')
            ->join('type_vegetals', 'especes.type_vegetal_id', '=', 'type_vegetals.id')
            ->orderBy('updated_at', 'desc')
            ->limit(10)
            ->get();

        return view('home')->with([
            'fiches' => $fiches
        ]);
    }
}
