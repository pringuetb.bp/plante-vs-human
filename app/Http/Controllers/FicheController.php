<?php

namespace App\Http\Controllers;

use App\Fiche;
use App\TypeVegetal;
use App\Http\Requests\StoreFicheRequest;
use App\Http\Requests\UpdateFicheRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class FicheController extends Controller
{
    /**
     * Affichage de toutes le fiches
     */
    public function index()
    {
        $types  = TypeVegetal::all();
        $fiches = DB::table('fiches')
            ->select(['fiches.*', 'especes.name', 'especes.slug', 'especes.type_vegetal_id', 'type_vegetals.name AS type'])
            ->join('especes', 'fiches.espece_id', '=', 'especes.id')
            ->join('type_vegetals', 'especes.type_vegetal_id', '=', 'type_vegetals.id')
            ->orderBy('name', 'asc')
            ->get();

        return view('fiche.fiches')->with([
            'types'  => $types,
            'fiches' => $fiches
        ]);
    }

    /*
     * Affiche les fiches recherchées
     */
    public function search(Request $request)
    {
        $search = $request->request->get('search');
        $types  = TypeVegetal::all();
        $fiches = DB::table('fiches')
            ->select(['fiches.*', 'especes.name', 'especes.slug', 'especes.type_vegetal_id', 'type_vegetals.name AS type'])
            ->join('especes', 'fiches.espece_id', '=', 'especes.id')
            ->join('type_vegetals', 'especes.type_vegetal_id', '=', 'type_vegetals.id')
            ->where('especes.slug', 'LIKE', "$search%")
            ->get();

        return view('fiche.fiches')->with([
            'types'  => $types,
            'fiches' => $fiches
        ]);
    }

    /*
     * Affiche les fiches recherchées
     */
    public function type(Request $request)
    {
        $type   = $request->request->get('type');
        $types  = TypeVegetal::all();
        if ($type != "all") {
            $fiches = DB::table('fiches')
            ->select(['fiches.*', 'especes.name', 'especes.slug', 'especes.type_vegetal_id', 'type_vegetals.name AS type'])
            ->join('especes', 'fiches.espece_id', '=', 'especes.id')
            ->join('type_vegetals', 'especes.type_vegetal_id', '=', 'type_vegetals.id')
            ->where('type_vegetals.id', '=', $type)
            ->get();
        } else {
            return redirect()->route('fiches');
        }

        return view('fiche.fiches')->with([
            'types'  => $types,
            'fiches' => $fiches
        ]);
    }

    /**
     * Page d'ajout d'une fiche
     */
    public function add()
    {
        if (Auth::check() && Auth::user()->role == 0) {
            $especes = DB::select('SELECT * FROM especes WHERE id NOT IN (SELECT espece_id FROM fiches)');

            return view('fiche.add')->with([
                'especes' => $especes
            ]);
        } else {
            return redirect()->route('dashbord');
        }
    }

    /**
     * Ajout d'une fiche
     */
    public function store(StoreFicheRequest $request)
    {
        if (Auth::check() && Auth::user()->role == 0) {
            $params = $request->validated();

            Storage::put('public/fiches', $params['image']);
            $params['image'] = 'fiches/'.$params['image']->hashName();

            Fiche::create($params);

            return redirect()->route('admin');
        } else {
            return redirect()->route('dashbord');
        }
    }

    /**
     * Affichage d'une fiche
     */
    public function details(string $slug)
    {
        $fiche = DB::table('fiches')
            ->select(['fiches.*', 'especes.name', 'especes.slug', 'especes.type_vegetal_id', 'type_vegetals.name AS type'])
            ->join('especes', 'fiches.espece_id', '=', 'especes.id')
            ->join('type_vegetals', 'especes.type_vegetal_id', '=', 'type_vegetals.id')
            ->where('especes.slug', '=', "$slug")
            ->first();

        return view('fiche.details')->with([
            'fiche'   => $fiche
        ]);
    }

    /**
     * Modification d'une fiche
     */
    public function update(UpdateFicheRequest $request, int $id)
    {
        if (Auth::check() && Auth::user()->role == 0) {
            $params = $request->validated();
            $fiche  = Fiche::findOrFail($id);

            if (isset($params['image']) && $params['image'] !== null) {
                Storage::delete('public/'.$fiche->image);
                Storage::put('public/fiches', $params['image']);
                $params['image'] = 'fiches/'.$params['image']->hashName();
            } else {
                $params['image'] = $fiche->image;
            }
            $fiche->update($params);
        }

        return back();
    }

    /**
     * Suppression d'une fiche
     */
    public function remove(int $id)
    {
        if (Auth::check() && Auth::user()->role == 0) {
            $fiche = Fiche::findOrFail($id);
            Storage::delete('public/'.$fiche->image);
            $fiche->delete();

            return redirect()->route('admin');
        } else {
            return redirect()->route('dashbord');
        }
    }
}
