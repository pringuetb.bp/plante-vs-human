<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateFicheRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'exposition'     => 'required|min:3',
            'type_sol'       => 'required|min:3',
            'taille'         => 'required|min:3',
            'arrosage_user'  => 'required|min:3',
            'arrosage_compt' => 'required|min:0',
            'conseils'       => 'nullable',
            'image'          => 'nullable|image'
        ];
    }
}
