<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeVegetal extends Model
{
    protected $table = 'type_vegetals';

    protected $fillable = [
        'name', 'slug'
    ];
}
