<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vegetal extends Model
{
    protected $table = 'vegetals';

    protected $fillable = [
        'name', 'date_plantation', 'user_id', 'espece_id'
    ];
}
