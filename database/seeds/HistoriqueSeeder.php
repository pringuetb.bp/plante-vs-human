<?php

use App\Historique;
use Illuminate\Database\Seeder;

class HistoriqueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $historiques = [
            [
                'date'       => '2020-09-15 15:42:30',
                'taille'     => '15',
                'arrose'     => '1',
                'engrais'    => 'null',
                'vegetal_id' => '1'
            ],
            [
                'date'       => '2020-09-15 15:42:30',
                'taille'     => '15',
                'arrose'     => 'null',
                'engrais'    => 'null',
                'vegetal_id' => '1'
            ],

            [
                'date'       => '2020-09-20 15:42:30',
                'taille'     => '25',
                'arrose'     => 'null',
                'engrais'    => 'null',
                'vegetal_id' => '1'
            ],
            [
                'date'       => '2020-10-15 15:42:30',
                'taille'     => '15',
                'arrose'     => '0',
                'engrais'    => '1',
                'vegetal_id' => '1'
            ],
            [
                'date'       => '2020-09-15 15:42:30',
                'taille'     => '26',
                'arrose'     => '1',
                'engrais'    => '1',
                'vegetal_id' => '2'
            ],
            [
                'date'       => '2020-09-15 15:42:30',
                'taille'     => '26',
                'arrose'     => 'null',
                'engrais'    => 'null',
                'vegetal_id' => '2'
            ],
            [
                'date'       => '2020-09-15 15:42:30',
                'taille'     => '26',
                'arrose'     => 'null',
                'engrais'    => '1',
                'vegetal_id' => '2'
            ],
            [
                'date'       => '2020-09-20 15:42:30',
                'taille'     => '35',
                'arrose'     => 'null',
                'engrais'    => 'null',
                'vegetal_id' => '2'
            ],
            [
                'date'       => '2020-09-15 15:42:30',
                'taille'     => '150',
                'arrose'     => 'null',
                'engrais'    => 'null',
                'vegetal_id' => '3'
            ],
        ];

        foreach ($historiques as $historique) {
            Historique::create($historique);
        }
    }
}
