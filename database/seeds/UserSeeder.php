<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 123456789
        $password = '$2y$10$FJdZ6TXVBJJCUIpoQSJWeO1dPDm/FunrNrQk1Wo2baTk9Xy2agZvm';
        $users = [
            [
                'name'     => 'Michel',
                'email'    => 'michel@gmail.com',
                'password' => $password,
                'role'     => '0'
            ],
            [
                'name'     => 'Laura',
                'email'    => 'laura@gmail.com',
                'password' => $password,
                'role'     => '1',
            ],
            [
                'name'     => 'Francois',
                'email'    => 'francois@gmail.com',
                'password' => $password,
                'role'     => '1'
            ]
        ];

        foreach ($users as $user)
        {
            User::create($user);
        }

    }
}
