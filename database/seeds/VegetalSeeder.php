<?php

use App\Vegetal;
use Illuminate\Database\Seeder;

class VegetalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vegetals = [
            [
                'name'            => 'Végétal 1',
                'date_plantation' => null,
                'user_id'         => '1',
                'espece_id'       => '1'
            ],
            [
                'name'            => 'Végétal 2',
                'date_plantation' => null,
                'user_id'         => '1',
                'espece_id'       => '2'
            ],
            [
                'name'            => 'Végétal 3',
                'date_plantation' => null,
                'user_id'         => '2',
                'espece_id'       => '3'
            ],
            [
                'name'            => 'Végétal 4',
                'date_plantation' => null,
                'user_id'         => '2',
                'espece_id'       => '4'
            ],
            [
                'name'            => 'Végétal 5',
                'date_plantation' => null,
                'user_id'         => '2',
                'espece_id'       => '5'
            ],
            [
                'name'            => 'Végétal 6',
                'date_plantation' => null,
                'user_id'         => '3',
                'espece_id'       => '6'
            ],
            [
                'name'            => 'Végétal 7',
                'date_plantation' => null,
                'user_id'         => '3',
                'espece_id'       => '7'
            ],
            [
                'name'            => 'Végétal 8',
                'date_plantation' => null,
                'user_id'         => '3',
                'espece_id'       => '8'
            ]
        ];

        foreach ($vegetals as $vegetal) {
            Vegetal::create($vegetal);
        }
    }
}
