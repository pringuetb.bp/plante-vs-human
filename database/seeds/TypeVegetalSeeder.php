<?php

use App\TypeVegetal;
use Illuminate\Database\Seeder;

class TypeVegetalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $typeVegetals = [
            [
                'name' => 'Orchidées',
                'slug' => 'orchidees'
            ],
            [
                'name' => 'Cactées',
                'slug' => 'cactees'
            ],
            [
                'name' => 'Palmier',
                'slug' => 'palmier'
            ],
            [
                'name' => 'Grimpantes et lianes',
                'slug' => 'grimpantes-et-lianes'
            ],
            [
                'name' => 'Bulbeuses',
                'slug' => 'bulbeuses'
            ]
        ];

        foreach ($typeVegetals as $typeVegetal) {
            TypeVegetal::create($typeVegetal);
        }
    }
}
