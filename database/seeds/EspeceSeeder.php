<?php

use App\Espece;
use Illuminate\Database\Seeder;

class EspeceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $especes = [
            [
                'name'            => 'Phalaenopsis',
                'slug'            => 'phalaenopsis',
                'type_vegetal_id' => '1'
            ],
            [
                'name'            => 'Cymbidium',
                'slug'            => 'cymbidium',
                'type_vegetal_id' => '1'
            ],
            [
                'name'            => 'Cambria',
                'slug'            => 'cambria',
                'type_vegetal_id' => '1'
            ],
            [
                'name'            => 'Opuntia ficus',
                'slug'            => 'opuntia-ficus',
                'type_vegetal_id' => '2'
            ],
            [
                'name'            => 'Parodia scopa',
                'slug'            => 'parodia-scopa',
                'type_vegetal_id' => '2'
            ],
            [
                'name'            => 'Kentia',
                'slug'            => 'kentia',
                'type_vegetal_id' => '3'
            ],
            [
                'name'            => 'Passiflore bleue',
                'slug'            => 'passiflore-bleue',
                'type_vegetal_id' => '4'
            ],
            [
                'name'            => 'Vigne vierge',
                'slug'            => 'vigne-vierge',
                'type_vegetal_id' => '4'
            ],
            [
                'name'            => 'Glaïeul',
                'slug'            => 'glaieul',
                'type_vegetal_id' => '5'
            ],
            [
                'name'            => 'Dahlia commun',
                'slug'            => 'dahlia-commun',
                'type_vegetal_id' => '5'
            ],
            [
                'name'            => 'Tulipe',
                'slug'            => 'tulipe',
                'type_vegetal_id' => '5'
            ],
            [
                'name'            => 'Crocus',
                'slug'            => 'crocus',
                'type_vegetal_id' => '5'
            ]
        ];

        foreach ($especes as $espece) {
            Espece::create($espece);
        }
    }
}
