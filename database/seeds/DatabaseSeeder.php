<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // https://www.aujardin.info/fiches/differents-types-plantes.php
        $this->call(UserSeeder::class);
        $this->call(TypeVegetalSeeder::class);
        $this->call(EspeceSeeder::class);
        $this->call(FicheSeeder::class);
        $this->call(VegetalSeeder::class);
        $this->call(HistoriqueSeeder::class);
    }
}
