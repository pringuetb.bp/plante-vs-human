<?php

use App\Fiche;
use Illuminate\Database\Seeder;

class FicheSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fiches = [
            [
                'exposition'     => 'soleil, mi-ombre',
                'type_sol'       => 'drainant, pauvre à rocailleux',
                'taille'         => 'de 1 à 1,50 m',
                'arrosage_user'  => 'arroser tous les 2 jours',
                'arrosage_compt' => 48,
                'conseils'       => "Lorsque la floraison est terminée, donc après les premières gelées et avant la reprise printanière, les Gauras seront rabattues à 10 cm du sol.
                Il faut veiller à garder de jeunes pieds nouveaux chaque année pour ne pas perdre l’espèce.
                Les variétés horticoles peuvent être bouturées, d’aout à novembre, l’intérêt étant de conserver exactement la forme sélectionnée.",
                'image'          => 'fiches/wv0Ny1cZ1dsp1e8wp1mZS8gRm9Sk0TZSHq3mXUjS.webp',
                'espece_id'      => '1'
            ],
            [
                'exposition'     => 'soleil',
                'type_sol'       => 'bien drainé, ordinaire, sablonneux',
                'taille'         => 'de 20 cm à 1m20 selon les espèces',
                'arrosage_user'  => 'arroser tous les jours',
                'arrosage_compt' => 24,
                'conseils'       => "Lorsque la floraison est terminée, donc après les premières gelées et avant la reprise printanière, les Gauras seront rabattues à 10 cm du sol.
                Il faut veiller à garder de jeunes pieds nouveaux chaque année pour ne pas perdre l’espèce.
                Les variétés horticoles peuvent être bouturées, d’aout à novembre, l’intérêt étant de conserver exactement la forme sélectionnée.",
                'image'          => 'fiches/xAeIPpoKyxJ6LLfUThMXjE2KrNClGMCWH6VZoe67.jpeg',
                'espece_id'      => '2'
            ],
            [
                'exposition'     => 'plein soleil',
                'type_sol'       => 'moyennement riche, léger, drainé',
                'taille'         => '30 cm',
                'arrosage_user'  => 'arroser toutes les heures',
                'arrosage_compt' => 1,
                'conseils'       => "Pour réussir la culture du pétunia, il faut qu'il puisse profiter du soleil, dans un endroit à l'abri du vent.
                Les semis de pétunias se font de février à mars, en terrine et au chaud. Repiquez les plants en godet, au stade de 3 ou 4 vraies feuilles, et gardez-les à l'abri. Mi-mai, mettez-les en terre, en potée ou en jardinière.
                Attention, les semis sont parfois capricieux.
                Lors de la plantation, espacez les pieds de pétunias de 20 à 30 cm en tous sens. Afin de provoquer la ramification des tiges, n'hésitez pas à les pincer.",
                'image'          => 'fiches/dGj70ANmjTjDfYC63yPdiKrGnHkaY3CutLU8DawS.jpeg',
                'espece_id'      => '3'
            ],
            [
                'exposition'     => 'soleil',
                'type_sol'       => 'ordinaire, légers, même secs ou rocailleux',
                'taille'         => 'jusqu\'à 6 m',
                'arrosage_user'  => 'arroser toutes les 6 heures',
                'arrosage_compt' => 6,
                'conseils'       => "",
                'image'          => 'fiches/QEfNQLxPLz2vvpz9uYeFjZqw8mfIlzBb3So8FKCQ.jpeg',
                'espece_id'      => '4'
            ],
            [
                'exposition'     => 'soleil, mi-ombre',
                'type_sol'       => 'plutôt riche en humus , bien drainé',
                'taille'         => 'Jusqu\'à 2 m',
                'arrosage_user'  => 'arroser toutes les 8 heures',
                'arrosage_compt' => 8,
                'conseils'       => "",
                'image'          => 'fiches/mxLLNJD1JB40QI58k0USPxiz7swWWomOSIkErIcn.webp',
                'espece_id'      => '5'
            ],
            [
                'exposition'     => 'soleil',
                'type_sol'       => 'fertile',
                'taille'         => 'jusqu\'à 100 m',
                'arrosage_user'  => 'arroser tous les jours',
                'arrosage_compt' => 24,
                'conseils'       => "",
                'image'          => 'fiches/bnJ3K0d2APaGZIvmWCz1iDvFkxf7WgXULQ5QOFJ5.jpeg',
                'espece_id'      => '6'
            ],
            [
                'exposition'     => 'soleil, mi-ombre',
                'type_sol'       => 'tout type de sol, privilégié un sol léger et drainant',
                'taille'         => '5 à 10m',
                'arrosage_user'  => 'arroser tous les jours',
                'arrosage_compt' => 24,
                'conseils'       => "",
                'image'          => 'fiches/pTCPfmYp6NV0j0T5LvTKyjgtJG455ySBRWtO1fqg.jpeg',
                'espece_id'      => '7'
            ],
            [
                'exposition'     => 'soleil, mi-ombre',
                'type_sol'       => 'ordinaire, riche en humus',
                'taille'         => '10m',
                'arrosage_user'  => 'arroser tous les jours',
                'arrosage_compt' => 24,
                'conseils'       => "",
                'image'          => 'fiches/9BKJ27tHCZLJGAZ4BhMljdDUNxyADIgWCFNek1fn.jpeg',
                'espece_id'      => '8'
            ],
            [
                'exposition'     => 'soleil',
                'type_sol'       => 'ordinaire, riche en humus, bien drainé',
                'taille'         => 'de 50 cm à 150 cm selon les variétés',
                'arrosage_user'  => 'arroser tous les jours',
                'arrosage_compt' => 24,
                'conseils'       => "",
                'image'          => 'fiches/9mT8f7FXIbu3d60q6Xx1nfGXOQ1cCeQNun1QorEk.jpeg',
                'espece_id'      => '9'
            ],
            [
                'exposition'     => 'plein soleil',
                'type_sol'       => 'riche en humus',
                'taille'         => 'de 30 à 150 cm',
                'arrosage_user'  => 'arroser tous les jours',
                'arrosage_compt' => 24,
                'conseils'       => "",
                'image'          => 'fiches/jIN5OktYFLgMPf6uvOvYWORY9KaeS5vWzsH6yWXs.jpeg',
                'espece_id'      => '10'
            ],
            [
                'exposition'     => 'plein soleil, mi-ombre, à l\'abri du vent',
                'type_sol'       => 'léger, sableux, drainé, riche',
                'taille'         => 'de 10 cm à 70 cm',
                'arrosage_user'  => 'arroser tous les jours',
                'arrosage_compt' => 24,
                'conseils'       => "",
                'image'          => 'fiches/PKbZwr1mkLxWcLyMG1S9DetEGkTHiWLfmPe077nD.jpeg',
                'espece_id'      => '11'
            ],
            [
                'exposition'     => 'soleil, mi-ombre',
                'type_sol'       => 'léger, sableux, riche, bien drainé',
                'taille'         => '15 cm',
                'arrosage_user'  => 'arroser tous les 2 jours',
                'arrosage_compt' => 48,
                'conseils'       => "",
                'image'          => 'fiches/X8kBu12sGf8BqAKQJdJTBQQ02lJF7wCcpBWCNbBI.jpeg',
                'espece_id'      => '12'
            ]
        ];

        foreach ($fiches as $fiche) {
            Fiche::create($fiche);
        }
    }
}
