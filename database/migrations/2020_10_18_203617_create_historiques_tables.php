<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoriquesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historiques', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vegetal_id');
            $table
                ->foreign('vegetal_id')
                ->references('id')
                ->on('vegetals')
                ->onDelete('CASCADE');
            $table->dateTime('date');
            $table->float('taille')->nullable();
            $table->boolean('arrose')->nullable();
            $table->boolean('engrais')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historiques');
    }
}
