<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFichesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fiches', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('espece_id');
            $table
                ->foreign('espece_id')
                ->references('id')
                ->on('especes')
                ->onDelete('CASCADE');
            $table->string('exposition')->nullable();
            $table->string('type_sol')->nullable();
            $table->string('taille')->nullable();
            $table->string('arrosage_user')->nullable();
            $table->bigInteger('arrosage_compt')->nullable();
            $table->text('conseils')->nullable();
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fiches');
    }
}
