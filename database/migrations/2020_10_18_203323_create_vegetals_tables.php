<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVegetalsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vegetals', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('espece_id');
            $table
                ->foreign('espece_id')
                ->references('id')
                ->on('especes')
                ->onDelete('CASCADE');
            $table->date('date_plantation')->nullable();
            $table->unsignedBigInteger('user_id');
            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vegetals');
    }
}
