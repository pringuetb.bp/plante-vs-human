# Cérès

## Pitch
Tout le monde n'a pas la main verte donc il est compliqué de maintenir une plante en vie.nous proposons une plateforme pour vous aidez a gérer vos plantes  

## Problèmes rencontré
Dans l'ensemble nous n'avons pas eu beaucoup de problème.
Erreur d'inatention dans le js et recuperation de données.
Cache de laravel

**BDD:** 
![alt text](https://cdn.discordapp.com/attachments/755762012574974073/768110529012301834/bdd.png "crud")

## Accounts
| Mail               | password  | role  |
|--------------------|-----------|-------|
| michel@gmail.com   | 123456789 | admin |
| laura@gmail.com    | 123456789 | user  |
| francois@gmail.com | 123456789 | user  |

## Project setup
Install vendors
```shell
composer install
```

Install nodes modules
```shell
yarn install
```
or
```shell
npm install
```

Copy file `.env.exemple` on `.env` then change the following line
```env
DB_DATABASE=ceres
```

Generate app key
```shell
php artisan key:generate
```

Generate storage link
```shell
php artisan storage:link
```

### load migrations
```shell
php artisan migrate
```

### load seed
```shell
php artisan db:seed
```

### webpack watcher
```shell
yarn watch
```
or
```shell
npm run watch
```

## Launch developpment server
```shell
php artisan serve
```
