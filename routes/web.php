<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/*
 * Home
 */
Route::get('/', 'HomeController@index')->name('home');

/*
 * User
 */
Route::get('/dashbord', 'UserController@dashbord')->name('dashbord');

Route::get('/profil', 'UserController@profil')->name('profil');

Route::put('/profil/update', 'UserController@update')->name('updateUser');

Route::get('/profil/delete', 'UserController@remove')->name('deleteUser');

/*
 * Fiches
 */
Route::get('/fiches', 'FicheController@index')->name('fiches');

Route::get('/fiches/search', 'FicheController@search')->name('searchFiches');

Route::get('/fiches/type', 'FicheController@type')->name('typeFiches');

Route::get('/fiches/add', 'FicheController@add')->name('addFiche');

Route::post('/fiches/add', 'FicheController@store')->name('storeFiche');

Route::get('/fiches/{slug}', 'FicheController@details')->name('detailsFiche');

Route::put('/fiches/{id}/update', 'FicheController@update')->name('updateFiche');

Route::get('/fiches/{id}/delete', 'FicheController@remove')->name('deleteFiche');

/*
 * Admin
 */
Route::get('/admin', 'AdminController@index')->name('admin');

/*
 * TypeVegatals
 */
Route::post('/type-vegetals/add', 'TypeVegetalController@store')->name('storeTypeVegetal');

Route::put('/type-vegetals/{id}/update', 'TypeVegetalController@update')->name('updateTypeVegetal');

Route::get('/type-vegetals/{id}/delete', 'TypeVegetalController@remove')->name('deleteTypeVegetal');

/*
 * Especes
 */
Route::post('/especes/add', 'EspeceController@store')->name('storeEspece');

Route::put('/especes/{id}/update', 'EspeceController@update')->name('updateEspece');

Route::get('/especes/{id}/delete', 'EspeceController@remove')->name('deleteEspece');

/*
 * Vegatals
 */
Route::post('/vegetals/add', 'VegetalController@store')->name('storeVegetal');

Route::put('/vegetals/{id}/update', 'VegetalController@update')->name('updateVegetal');

Route::get('/vegetals/{id}/delete', 'VegetalController@remove')->name('deleteVegetal');


/*
 * Historique
 */
Route::post('/historique/add-size', 'HistoriqueController@storeSize')->name('storeSize');

Route::get('/historique/{id}', 'HistoriqueController@index')->name('historique');

Route::post('/historique/{id}/add-water', 'HistoriqueController@storeWater')->name('storeWater');

Route::post('/historique/{id}/add-fertilizer', 'HistoriqueController@storeFertilizer')->name('storeFertilizer');

Route::put('/historique/{id}/update', 'HistoriqueController@update')->name('updateHistorique');

Route::get('/historique/{id}/delete', 'HistoriqueController@remove')->name('deleteHistorique');
