@extends('layouts.app')

@section('content')
    <div class="d-flex flex-column bd-highlight mb-3">
        <div class="card shadow mb-4">
            <div class="card-body">
                <h1 class="mb-0 text-gray-800 pb-2">Qu'est ce que Cérès</h1>
                <p class="m-0">
                    Cérès est une plartefome gratuite d'aide à l'entretien de vos plantes. Elle vous permet une gestion simple de l'ensemble des plantes de votre maison.<br>
                    Cérès vous rappelle quand il faut arroséee vaut plante. Vous trouverez aussi des conseil pour entretenir votre plante tel que: l'exposition, le type de sol etc ...
                </p>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column bd-highlight mb-3">
        <h2 class="mb-0 text-gray-800">Les 10 dèrnières fiches</h2>
    </div>

    @include('_inc.ficheLayout')
@endsection
