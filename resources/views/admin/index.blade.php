@extends('layouts.app')

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="mb-0 text-gray-800">Administration</h1>
    </div>

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h2 class="mb-0 text-gray-800">Statistiques</h2>
    </div>

    <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 row-cols-xl-5">

        <div class="col pb-4">
            <div class="card shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-uppercase mb-1">Utilisateurs</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $nbUsers }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-user fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col pb-4">
            <div class="card shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-uppercase mb-1">Types de végétaux</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ sizeof($typeVegetals) }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-compass fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col pb-4">
            <div class="card shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-uppercase mb-1">Espèces</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ sizeof($especes) }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-leaf fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col pb-4">
            <div class="card shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-uppercase mb-1">Fiches</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ sizeof($fiches) }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-file fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col pb-4">
            <div class="card shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-uppercase mb-1">Espèces sans fiche</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ sizeof($noFiche) }}</div>
                        </div>
                        <div class="col-auto">
                            @if (sizeof($noFiche) > 0)
                                <i class="fas fa-exclamation-triangle fa-2x text-warning"></i>
                            @else
                                <i class="fas fa-check fa-2x text-success"></i>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h2 class="mb-0 text-gray-800">Données</h2>
    </div>

    <div class="row">

        <div class="col-12 col-lg-5">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Types de végétaux</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                            {{-- <div class="dropdown-header">Dropdown Header:</div> --}}
                            <a class="dropdown-item" data-toggle="modal" data-target="#typeVegetalModal"><i class="fas fa-plus pr-2"></i>Ajouter</a>
                        </div>
                    </div>
                </div>
                <div class="card-body p-0">
                    <table class="table table-striped m-0">
                        <thead>
                            <tr>
                                <th class="align-middle text-center">#</th>
                                <th class="align-middle text-center">Libellé</th>
                                <th class="align-middle text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($typeVegetals as $type)
                                <tr>
                                    <form method="post" action="{{ route('updateTypeVegetal', $type->id) }}">
                                        @csrf
                                        @method('put')

                                        <td class="align-middle text-center">{{ $type->id }}</td>
                                        <td class="align-middle text-center py-2">
                                            <div class="editAction">{{ $type->name }}</div>
                                            <input type="text" class="form-control editAction d-none" name="name" value="{{ $type->name }}" minlength="3" required>
                                        </td>
                                        <td class="align-middle text-center py-2">
                                            <button type="submit" class="btn btn-circle btn-outline-success editAction d-none"><i class="fas fa-save"></i></button>
                                            <button type="button" onclick="editAction($(this))" class="btn btn-circle btn-outline-danger editAction d-none"><i class="fas fa-times"></i></button>
                                            <button type="button" onclick="editAction($(this))" class="btn btn-circle btn-outline-warning btnAction"><i class="fas fa-edit"></i></button>
                                            <button type="button" class="btn btn-circle btn-outline-danger btnAction" data-toggle="modal" data-target="#deleteModal" data-text="{{ $type->name }}" data-href="{{ route('deleteTypeVegetal', $type->id) }}"><i class="fas fa-trash"></i></button>
                                        </td>
                                    </form>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-7">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Espèces</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                            {{-- <div class="dropdown-header">Dropdown Header:</div> --}}
                            <a class="dropdown-item" data-toggle="modal" data-target="#especeModal"><i class="fas fa-plus pr-2"></i>Ajouter</a>
                        </div>
                    </div>
                </div>
                <div class="card-body p-0">
                    <table class="table table-striped m-0">
                        <thead>
                            <tr>
                                <th class="align-middle text-center">#</th>
                                <th class="align-middle text-center">Libellé</th>
                                <th class="align-middle text-center">Type</th>
                                <th class="align-middle text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($especes as $espece)
                                <tr @if (in_array($espece->id, array_column($noFiche, 'id'), true)) class="bg-gradient-warning opacity-3" @endif>
                                    <form method="post" action="{{ route('updateEspece', $espece->id) }}">
                                        @csrf
                                        @method('put')

                                        <td class="align-middle text-center @if (in_array($espece->id, array_column($noFiche, 'id'), true)) text-white @endif">{{ $espece->id }}</td>
                                        <td class="align-middle text-center py-2">
                                            <div class="@if (in_array($espece->id, array_column($noFiche, 'id'), true)) text-white @endif editAction">{{ $espece->name }}</div>
                                            <input type="text" class="form-control editAction d-none" name="name" value="{{ $espece->name }}" minlength="3" required>
                                        </td>
                                        <td class="align-middle text-center py-2">
                                            <div class="@if (in_array($espece->id, array_column($noFiche, 'id'), true)) text-white @endif editAction">{{ $espece->type }}</div>
                                            <div class="input-group editAction d-none">
                                                <select class="form-control" name="type_vegetal_id" required>
                                                    @foreach ($typeVegetals as $type)
                                                        <option value="{{ $type->id }}" @if ($type->id === $espece->type_vegetal_id) selected @endif>{{ $type->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </td>
                                        <td class="align-middle text-center py-2">
                                            <button type="submit" class="btn btn-circle btn-outline-success editAction d-none"><i class="fas fa-save"></i></button>
                                            <button type="button" onclick="editAction($(this))" class="btn btn-circle btn-outline-danger editAction d-none"><i class="fas fa-times"></i></button>
                                            <button type="button" onclick="editAction($(this))" class="btn btn-circle btn-outline-warning btnAction"><i class="fas fa-edit"></i></button>
                                            <button type="button" class="btn btn-circle btn-outline-danger btnAction" data-toggle="modal" data-target="#deleteModal" data-text="{{ $espece->name }}" data-href="{{ route('deleteEspece', $espece->id) }}"><i class="fas fa-trash"></i></button>
                                        </td>
                                    </form>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h2 class="mb-0 text-gray-800">Fiches</h2>
    </div>

    @include('_inc.ficheLayout')

    @if (sizeof($noFiche) > 0)
        <div class="d-flex justify-content-end">
            <a class="btn btn-circle btn-outline-primary" href="{{route('addFiche')}}"><i class="fas fa-plus"></i></a>
        </div>
    @endif

    <div class="modal fade" id="typeVegetalModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="tvModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form method="post" action="{{route('storeTypeVegetal')}}">
                @csrf
                @method('post')

                <div class="modal-content">
                    <div class="modal-header py-2">
                        <h5 class="modal-title" id="tvModal">Ajouter un type de végétal</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group required">
                            <label>Libellé</label>
                            <input type="text" class="form-control" name="name" required>
                        </div>
                    </div>
                    <div class="modal-footer py-1">
                        <button type="submit" class="btn btn-circle btn-outline-success"><i class="fas fa-check"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="especeModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="eModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form method="post" action="{{route('storeEspece')}}">
                @csrf
                @method('post')

                <div class="modal-content">
                    <div class="modal-header py-2">
                        <h5 class="modal-title" id="eModal">Ajouter une espèce</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group required">
                            <label>Libellé</label>
                            <input type="text" class="form-control" name="name" required>
                        </div>
                        <div class="form-group required">
                            <label>Type</label>
                            <select class="form-control" name="type_vegetal_id" required>
                                @foreach ($typeVegetals as $type)
                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer py-1">
                        <button type="submit" class="btn btn-circle btn-outline-success"><i class="fas fa-check"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
