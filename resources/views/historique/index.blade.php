@extends('layouts.app')

@section('content')
    <div class="d-flex">
        <div class="d-sm-flex align-items-center justify-content-between mb-4 pr-2">
            <h1 class="mb-0 text-gray-800">Historique de {{ $vegetal->name }}</h1>
        </div>
        <button type="button" class="btn btn-circle btn-outline-warning m-1" data-toggle="modal" data-target="#editVegetalModal"><i class="fas fa-edit"></i></button>
        <button type="button" data-toggle="modal" data-target="#deleteModal" data-text="{{ $vegetal->name }}" data-href="{{ route('deleteVegetal', ['id' => $vegetal->id]) }}" class="btn btn-circle btn-outline-danger m-1"><i class="fas fa-trash"></i></button>
    </div>

    <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">

        <div class="col pb-4">
            <div class="card shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center h-100">
                        <div class="col mr-2 h-100">
                            <div class="text-xs font-weight-bold text-uppercase mb-1">Espèce</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $vegetal->espece }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-leaf fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col pb-4">
            <div class="card shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center h-100">
                        <div class="col mr-2 h-100">
                            <div class="text-xs font-weight-bold text-uppercase mb-1">Type</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $vegetal->type }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-spa fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if (!empty($vegetal->taille))
            <div class="col pb-4">
                <div class="card shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center h-100">
                            <div class="col mr-2 h-100">
                                <div class="text-xs font-weight-bold text-uppercase mb-1">Taille actuelle</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $vegetal->taille }} cm</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-ruler fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if (!empty($vegetal->date_plantation))
            <div class="col pb-4">
                <div class="card shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center h-100">
                            <div class="col mr-2 h-100">
                                <div class="text-xs font-weight-bold text-uppercase mb-1">Date de plantation</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{ date_format(new \DateTime($vegetal->date_plantation), 'd M Y') }}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-calendar-alt fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if (!empty($vegetal->arrosee))
            <div class="col pb-4">
                <div class="card shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center h-100">
                            <div class="col mr-4 h-100">
                                <div class="text-xs font-weight-bold text-uppercase mb-1">Prochain arrosage</div>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-calendar-alt fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    </div>

    <div class="row">

        <div class="col-12 col-lg-5">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Historique</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                            {{-- <div class="dropdown-header">Dropdown Header:</div> --}}
                            <button type="button" data-toggle="modal" data-target="#addSizeModal" data-vegetal="{{ $vegetal->id }}" class="dropdown-item text-dark"><i class="fas fa-ruler pr-2"></i>Nouvelle taille</button>
                            <form method="post" action="{{ route('storeWater', ['id' => $vegetal->id]) }}">
                                @csrf

                                <button type="submit" class="dropdown-item text-info"><i class="fas fa-tint pr-2"></i>Arroser</button>
                            </form>
                            <form method="post" action="{{ route('storeFertilizer', ['id' => $vegetal->id]) }}">
                                @csrf

                                <button type="submit" class="dropdown-item text-success"><i class="fas fa-arrow-up pr-2"></i>Ajouter de l'engrais</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card-body p-0">
                    @if (!empty($histories[0]))
                        <table class="table table-striped m-0">
                            <thead>
                                <tr>
                                    <th class="align-middle text-center">Date</th>
                                    <th class="align-middle text-center">Libellé</th>
                                    <th class="align-middle text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($histories as $history)
                                    <tr>
                                        <td class="align-middle text-center">
                                            @if ($history->arrose !== null)
                                                {{ date_format(new \DateTime($history->date), 'd M Y à H:i') }}
                                            @else
                                                {{ date_format(new \DateTime($history->date), 'd M Y') }}
                                            @endif
                                        </td>
                                        <td class="align-middle text-center py-2">
                                            @if ($history->arrose !== null)
                                                Arrosage
                                            @endif
                                            @if ($history->engrais !== null)
                                                Engrais
                                            @endif
                                            @if ($history->taille !== null)
                                                Taille
                                            @endif
                                        </td>
                                        <td class="align-middle text-center py-2">
                                            <button type="button" class="btn btn-circle btn-outline-danger" data-toggle="modal" data-target="#deleteModal" data-text="@if ($history->arrose !== null)Arrosage @endif @if ($history->engrais !== null)Engrais @endif @if ($history->taille !== null)Taille @endif" data-href="{{ route('deleteHistorique', ['id' => $history->id]) }}"><i class="fas fa-trash"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="text-center p-4">Aucun</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-7">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Taille</h6>
                </div>
                <div class="card-body p-0">
                    @if (!empty($vegetal->taille))
                        <canvas id="myChart"></canvas>
                    @else
                        <div class="text-center p-4">Aucune</div>
                    @endif
                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="editVegetalModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="eVModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form method="post" action="{{ route('updateVegetal', ['id' => $vegetal->id]) }}">
                @csrf
                @method('put')

                <div class="modal-content">
                    <div class="modal-header py-2">
                        <h5 class="modal-title" id="eVModal">Modifier "{{ $vegetal->name }}"</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group required">
                            <label>Nom</label>
                            <input type="text" class="form-control" name="name" value="{{ $vegetal->name }}" required>
                        </div>
                        <div class="form-group required">
                            <label>Espèce</label>
                            <select class="form-control" name="espece_id" required>
                                @foreach ($especes as $espece)
                                    <option value="{{ $espece->id }}" @if ($espece->id === $vegetal->espece_id) selected @endif>{{ $espece->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Date de plantation</label>
                            <input class="form-control" type="date" name="date_plantation" value="{{ $vegetal->date_plantation }}">
                        </div>
                    </div>
                    <div class="modal-footer py-1">
                        <button type="submit" class="btn btn-circle btn-outline-success"><i class="fas fa-check"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript">
        function docReady(fn) {
            // see if DOM is already available
            if (document.readyState === "complete" || document.readyState === "interactive") {
                // call on next available tick
                setTimeout(fn, 1);
            } else {
                document.addEventListener("DOMContentLoaded", fn);
            }
        }

        docReady(function() {
            function diff_hours(now, aroser) {
                var diffs =((now.getTime() - aroser.getTime()) / 1000)/3600;
                return Math.abs(diffs);
            }

            function moyenne(diff) {
                var calcul = (diff / '{{ $fiches[0]->arrosage_compt }}') * 100;
                return calcul;
            }

            var aroser = '{{date_format(new \DateTime($arose[0]->date), 'M d,Y H:i:s')}}';
            var now = '{{date_format(new \DateTime($day), 'M d,Y H:i:s')}}';
            aroser = new Date(aroser);
            now = new Date(now);

            var diff = diff_hours(aroser, now);
            var pourcent = moyenne(diff);

            document.getElementsByClassName("progress-bar")[0].style.width = pourcent+"%";

            var ctx = document.getElementById('myChart').getContext('2d');
            var chart = new Chart(ctx, {
                // The type of chart we want to create
                type: 'line',

                // The data for our dataset
                data: {
                    labels: [
                        @foreach($tailles as $taille)
                            "{{ date_format(new \DateTime($taille->date), 'd/m/Y') }}",
                        @endforeach
                    ],
                    datasets: [{
                        label: 'Taille de {{ $vegetal->name }}',
                        backgroundColor: 'rgb(57, 108, 99)',
                        borderColor: 'rgb(251, 121, 87)',
                        data: [
                            @foreach($tailles as $taille)
                                {{ ($taille->taille) }},
                            @endforeach
                        ]
                    }]
                },

                // Configuration options go here
                options: {}
            });
        });
    </script>
@endsection
