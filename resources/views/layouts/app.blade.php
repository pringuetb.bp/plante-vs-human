<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body id="wrapper">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    @include('_inc.sidebar')
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">

            <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle bg-light ml-2 mt-2">
                <i class="fa fa-bars"></i>
            </button>

            <main class="container-fluid pb-3 pt-md-3">
                @yield('content')

                <div class="modal fade" id="logoutModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="loutModal" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-body p-2">
                                <h5 class="modal-title text-center mb-2" id="loutModal">Prêt à partir ?</h5>
                                <div class="d-flex justify-content-between">
                                    <a class="btn btn-circle btn-outline-danger" data-dismiss="modal"><i class="fas fa-times"></i></a>
                                    <a class="btn btn-circle btn-outline-success pl-auto" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fas fa-check"></i></a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="deleteModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="dModal" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-body p-2">
                                <h5 class="modal-title text-center mb-2" id="dModal">Etes-vous sur de vouloir supprimer "<span id="dText"></span>" ?</h5>
                                <div class="d-flex justify-content-between">
                                    <a class="btn btn-circle btn-outline-danger" data-dismiss="modal"><i class="fas fa-times"></i></a>
                                    <a class="btn btn-circle btn-outline-success pl-auto" id="dSubmit"><i class="fas fa-check"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="addSizeModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="aSizeModal" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <form method="post" action="{{ route('storeSize') }}">
                            @csrf

                            <input type="hidden" name="vegetal_id" id="vegetal_id">
                            <div class="modal-content">
                                <div class="modal-header py-2">
                                    <h5 class="modal-title" id="aSizeModal">Nouvelle taille</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group required">
                                        <label>Taille</label>
                                        <div class="input-group">
                                            <input type="number" min="0" step="0.1" class="form-control" name="taille" aria-describedby="size" required>
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="size">cm</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer py-1">
                                    <button type="submit" class="btn btn-circle btn-outline-success"><i class="fas fa-check"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </main>
        </div>
    </div>

    <script type="text/javascript">
        function editAction(event) {
            $('.btnAction').each(function() {
                if ($(this).hasClass('d-none')) {
                    $(this).removeClass('d-none');
                } else {
                    $(this).addClass('d-none');
                }
            });

            let elements = null;
            if (event != null) {
                elements = event.parent().parent().find('.editAction');
            } else {
                elements = $('.editAction');
            }
            elements.each(function() {
                if ($(this).hasClass('d-none')) {
                    $(this).removeClass('d-none');
                } else {
                    $(this).addClass('d-none');
                }
            });
        }
    </script>
</body>
</html>
