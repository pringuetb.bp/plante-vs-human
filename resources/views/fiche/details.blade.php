@extends('layouts.app')

@section('content')
    @if (!empty($fiche))
        <form method="post" action="{{ route('updateFiche', $fiche->id) }}" enctype="multipart/form-data">
            @csrf
            @method('put')

            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="mb-0 text-gray-800">{{ $fiche->name }}</h1>
            </div>

            <div class="row">
                <div class="col-12 col-lg-4 col-xl-3 w-100 mb-4 mb-lg-0">
                    <div class="card shadow mb-4 @if ($fiche->image == '') d-none editAction @endif">
                        <img class="card-img @if ($fiche->image == '') d-none @endif" src="{{ asset('storage/'.$fiche->image) }}">
                        <div class="d-flex">
                            <div class="custom-file m-3 editAction d-none">
                                <input type="file" name="image" class="custom-file-input" id="inputFile">
                                <label class="custom-file-label" for="inputFile">Sélectionnez une image</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-8 col-xl-9">
                    <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">

                        <div class="col pb-4">
                            <div class="card shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center h-100">
                                        <div class="col mr-2 h-100">
                                            <div class="text-xs font-weight-bold text-uppercase mb-1">Type</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $fiche->type }}</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-spa fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col pb-4">
                            <div class="card shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center h-100">
                                        <div class="col mr-2 h-100">
                                            <div class="text-xs font-weight-bold text-uppercase mb-1">Exposition</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800 editAction">{{ $fiche->exposition }}</div>
                                            <input type="text" class="form-control editAction d-none" name="exposition" value="{{ old('exposition') ? old('exposition') : $fiche->exposition }}" minlength="3" required>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-map-marker fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col pb-4">
                            <div class="card shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center h-100">
                                        <div class="col mr-2 h-100">
                                            <div class="text-xs font-weight-bold text-uppercase mb-1">Type de sol</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800 editAction">{{ $fiche->type_sol }}</div>
                                            <input type="text" class="form-control editAction d-none" name="type_sol" value="{{ old('type_sol') ? old('type_sol') : $fiche->type_sol }}" minlength="3" required>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-map fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col pb-4">
                            <div class="card shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center h-100">
                                        <div class="col mr-2 h-100">
                                            <div class="text-xs font-weight-bold text-uppercase mb-1">Taille</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800 editAction">{{ $fiche->taille }}</div>
                                            <input type="text" class="form-control editAction d-none" name="taille" value="{{ old('taille') ? old('taille') : $fiche->taille }}" minlength="3" required>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-ruler fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col pb-4">
                            <div class="card shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center h-100">
                                        <div class="col mr-2 h-100">
                                            <div class="text-xs font-weight-bold text-uppercase mb-1">Pérode d'arrosage</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800 editAction">{{ $fiche->arrosage_user }}</div>
                                            <input type="text" class="form-control editAction d-none" name="arrosage_user" value="{{ old('arrosage_user') ? old('arrosage_user') : $fiche->arrosage_user }}" minlength="3" required>
                                            @if (Auth::check() && Auth::user()->role == 0)
                                                <div class="text-xs font-weight-bold text-uppercase mb-1 mt-2">Alertes utilisateur</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800 editAction">{{ $fiche->arrosage_compt }} h</div>
                                            @endif
                                            <div class="input-group editAction d-none">
                                                <input type="number" min="0" step="1" class="form-control" name="arrosage_compt" value="{{ old('arrosage_compt') ? old('arrosage_compt') : $fiche->arrosage_compt }}" minlength="3" required>
                                                <div class="input-group-append">
                                                    <div class="input-group-text">h</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-calendar-alt fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="card shadow mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Conseils</h6>
                        </div>
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="editAction">{{ $fiche->conseils }}</div>
                                    <textarea class="form-control editAction d-none" name="conseils" rows="10">{{ old('conseils') ? old('conseils') : $fiche->conseils }}</textarea>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-hand-holding-heart fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="d-flex justify-content-end">
                @if (Auth::check() && Auth::user()->role == 0)
                    <div class="mr-4">
                        <button type="submit" class="btn btn-circle btn-outline-success mr-2 editAction d-none"><i class="fas fa-save"></i></button>
                        <button type="button" onclick="editAction()" class="btn btn-circle btn-outline-danger editAction d-none"><i class="fas fa-times"></i></button>
                        <button type="button" onclick="editAction()" class="btn btn-circle btn-outline-warning mr-2 btnAction"><i class="fas fa-edit"></i></button>
                        <button type="button" data-toggle="modal" data-target="#validationModal" class="btn btn-circle btn-outline-danger btnAction"><i class="fas fa-trash"></i></button>
                    </div>
                @endif
                <div>
                    <u>Sortie :</u> {{ date_format(new \DateTime($fiche->created_at), 'M Y') }}<br>
                    <u>Mise à jour :</u> {{ date_format(new \DateTime($fiche->updated_at), 'M Y') }}
                </div>
            </div>
        </form>

        @if (Auth::check() && Auth::user()->role == 0)
            <div class="modal fade" id="validationModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="validModal" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-body p-2">
                            <h5 class="modal-title text-center mb-2" id="validModal">Etes-vous sûr de vouloir supprimer cette fiche ?</h5>
                            <div class="d-flex justify-content-between">
                                <a class="btn btn-circle btn-outline-danger" data-dismiss="modal"><i class="fas fa-times"></i></a>
                                <a class="btn btn-circle btn-outline-success pl-auto" href="{{ route('deleteFiche', $fiche->id) }}"><i class="fas fa-check"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @else
        Aucun résultat
    @endif
@endsection
