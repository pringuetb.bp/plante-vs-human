@extends('layouts.app')

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="mb-0 text-gray-800">Fiches</h1>
    </div>

    <div class="mb-4 row justify-content-between">
        <form class="col-12 col-md-9 mb-2 mb-md-0" action="{{ route('searchFiches') }}">
            <div class="input-group">
                <input type="text" name="search" class="form-control" placeholder="Rechercher" aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit">
                        <i class="fas fa-search fa-sm"></i>
                    </button>
                </div>
            </div>
        </form>

        <form class="col-12 col-md-3" action="{{ route('typeFiches') }}">
            <div class="input-group">
                <select class="form-control" name="type" required>
                    <option value="all">Toutes</option>
                    @foreach ($types as $type)
                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                    @endforeach
                </select>
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit">
                        <i class="fas fa-search fa-sm"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>

    @if (!empty($fiches[0]))
        @include('_inc.ficheLayout')
    @else
        Aucun résultat
    @endif
@endsection
