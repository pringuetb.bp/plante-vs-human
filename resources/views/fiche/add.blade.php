@extends('layouts.app')

@section('content')
    <form method="post" action="{{ route('addFiche') }}" enctype="multipart/form-data">
        @csrf

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="mb-0 text-gray-800">Ajouter une fiche</h1>
        </div>

        <div class="row mb-4">
            <div class="col-12 col-lg-4 col-xl-3 w-100 mb-4 mb-lg-0">
                <div class="card shadow mb-4">
                    <div class="d-flex">
                        <div class="custom-file m-3">
                            <input type="file" name="image" class="custom-file-input" id="inputFile" required>
                            <label class="custom-file-label" for="inputFile">Sélectionnez une image</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-8 col-xl-9">
                <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">

                    <div class="col pb-4">
                        <div class="card shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-uppercase mb-1">Espèce</div>
                                        <div class="input-group">
                                            <select class="form-control" name="espece_id">
                                                @foreach ($especes as $espece)
                                                    <option value="{{ $espece->id }}" @if (old('espece_id') && old('espece_id') == $espece->id) selected @endif>{{ $espece->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-spa fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col pb-4">
                        <div class="card shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-uppercase mb-1">Exposition</div>
                                        <input type="text" class="form-control" name="exposition" value="{{ old('exposition') }}" minlength="3" required>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-map-marker fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col pb-4">
                        <div class="card shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-uppercase mb-1">Type de sol</div>
                                        <input type="text" class="form-control" name="type_sol" value="{{ old('type_sol') }}" minlength="3" required>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-map fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col pb-4">
                        <div class="card shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-uppercase mb-1">Taille</div>
                                        <input type="text" class="form-control" name="taille" value="{{ old('taille') }}" minlength="3" required>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-ruler fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col pb-4">
                        <div class="card shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-uppercase mb-1">Pérode d'arrosage</div>
                                        <input type="text" class="form-control" name="arrosage_user" value="{{ old('arrosage_user') }}" minlength="3" required>
                                        <div class="text-xs font-weight-bold text-uppercase mb-1 mt-2">Alertes utilisateur</div>
                                        <div class="input-group">
                                            <input type="number" min="0" step="1" class="form-control" name="arrosage_compt" value="{{ old('arrosage_compt') }}" minlength="3" required>
                                            <div class="input-group-append">
                                                <div class="input-group-text">h</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-calendar-alt fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="card shadow">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Conseils</h6>
                    </div>
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <textarea class="form-control" name="conseils" rows="10">{{ old('conseils') }}</textarea>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-hand-holding-heart fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="d-flex justify-content-end">
            <div class="mr-3">
                <button type="submit" class="btn btn-circle btn-outline-success mr-2"><i class="fas fa-save"></i></button>
                <button type="reset" class="btn btn-circle btn-outline-danger"><i class="fas fa-times"></i></button>
            </div>
        </div>
    </form>
@endsection
