@extends('layouts.emptyBase')

@section('content')
<div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
                <div class="col-lg-7">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Créez un compte !</h1>
                        </div>
                        <form method="POST" action="{{ route('register') }}" class="user">
                            @csrf
                            <div class="form-group">
                                <input id="name" type="text" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus class="form-control form-control-user @error('name') is-invalid @enderror" placeholder="Nom">
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input id="email" type="email" name="email" value="{{ old('email') }}" required autocomplete="email" class="form-control form-control-user @error('email') is-invalid @enderror" placeholder="Email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <input id="password" type="password" name="password" required autocomplete="new-password" class="form-control form-control-user @error('password') is-invalid @enderror" placeholder="Mot de passer">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-sm-6">
                                    <input id="password-confirm" type="password" name="password_confirmation" required autocomplete="new-password" class="form-control form-control-user" placeholder="Confirmer le mot de passe">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-user btn-block">S'inscrire</button>
                        </form>
                        <hr>
                        <div class="text-center">
                            <a class="small" href="{{ route('login') }}">Vous avez déjà un compte ? Connectez-vous !</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
