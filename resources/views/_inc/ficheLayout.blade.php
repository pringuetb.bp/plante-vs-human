<div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">
    @foreach ($fiches as $fiche)
        <div class="col mb-4" >
            <a href="{{ route('detailsFiche', ['slug' => $fiche->slug ? $fiche->slug : $fiche->espece->slug ]) }}" class="card shadow h-100 link-unstyled">
                <div class="card-body">
                    <div class="no-gutters align-items-center">
                        <div class="font-weight-bold text-uppercase mb-1">{{ $fiche->name }}</div>
                        <div class="mb-0 text-gray-800">
                            <strong>Type :</strong> {{ $fiche->type }}<br>
                            <strong>Taille :</strong> {{ $fiche->taille }}<br>
                            <div class="d-flex justify-content-end">{{ date_format(new \DateTime($fiche->updated_at), 'M Y') }}</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    @endforeach
</div>
