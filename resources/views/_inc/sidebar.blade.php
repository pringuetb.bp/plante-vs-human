<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

    <a class="sidebar-brand d-flex align-items-center justify-content-center mt-3" href="{{ url('/') }}">
        <div class="sidebar-brand-icon">
            <i class="fas fa-seedling"></i>
            <div class="titre">Cérès</div>
        </div>
        <div class="sidebar-brand-text mx-3"></div>
    </a>

    <hr class="sidebar-divider my-0">

    @if (Auth::check())
        <li class="nav-item">
            <a class="nav-link" href="{{ route('dashbord') }}">
                <i class="fas fa-tachometer-alt"></i>
                <span>Tableau de bord</span>
            </a>
        </li>
    @else
        <li class="nav-item">
            <a class="nav-link" href="{{ route('home') }}">
                <i class="fas fa-home"></i>
                <span>Accueil</span>
            </a>
        </li>
    @endif

    <li class="nav-item">
        <a class="nav-link" href="{{ route('fiches') }}">
            <i class="fas fa-info-circle"></i>
            <span>Fiches</span>
        </a>
    </li>

    @if (Auth::check())
        <li class="nav-item">
            <a class="nav-link" href="{{ route('profil') }}">
                <i class="fas fa-user"></i>
                <span>Profil</span>
            </a>
        </li>

        @if (Auth::user()->role == 0)
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin') }}">
                    <i class="fas fa-cogs"></i>
                    <span>Administration</span>
                </a>
            </li>
        @endif

        <hr class="sidebar-divider my-0">

        <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="modal" data-target="#logoutModal">
                <i class="fas fa-sign-out-alt"></i>
                <span>Déconnexion</span>
            </a>
        </li>
    @else
        <hr class="sidebar-divider my-0">

        <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}">
                <i class="fas fa-sign-in-alt"></i>
                <span>Connexion</span>
            </a>
        </li>
    @endif

</ul>
