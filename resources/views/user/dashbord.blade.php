@extends('layouts.app')

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="mb-0 text-gray-800">Tableau de bord de {{ Auth::user()->name }}</h1>
    </div>

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h2 class="mb-0 text-gray-800">Mes plantes</h2>
    </div>

    <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">
        @foreach($vegetals as $vegetal)
            <div class="col mb-4">
                <div class="card shadow h-100">
                    <div class="card-body">
                        <div class="font-weight-bold text-uppercase mb-1">{{ $vegetal->name }}</div>
                        <div class="row no-gutters align-items-center">
                            <div class="col text-gray-800">
                                <strong>Espèce :</strong> {{ $vegetal->espece }}<br>
                                <strong>Type :</strong> {{ $vegetal->type }}<br>
                                @if (!empty($vegetal->date_plantation))
                                    <strong>Plantation :</strong> {{ date_format(new \DateTime($vegetal->date_plantation), 'd M Y') }}<br>
                                @endif
                                <strong>Historique :</strong>
                                <ul class="pl-4">
                                    @if (!empty($vegetal->taille))
                                        <li>
                                            <strong>Taille :</strong> {{ $vegetal->taille }} cm
                                        </li>
                                    @endif
                                    @if (!empty($vegetal->arrosee))
                                        <li>
                                            <strong>Arrosage :</strong> {{ date_format(new \DateTime($vegetal->arrosee), 'd M Y à H:i') }}
                                        </li>
                                    @endif
                                    @if (!empty($vegetal->engrais))
                                        <li>
                                            <strong>Engrais :</strong> {{ date_format(new \DateTime($vegetal->engrais), 'd M Y') }}
                                        </li>
                                    @endif
                                </ul>
                            </div>
                            <div class="col-auto d-flex flex-column">
                                <div class="d-flex">
                                    <a href="{{ route('detailsFiche', ['slug' => $vegetal->especeSlug ]) }}" class="btn btn-circle btn-outline-secondary m-1" title="Fiche"><i class="fas fa-file"></i></a>
                                    <a href="{{ route('historique', ['id' => $vegetal->id ]) }}" class="btn btn-circle btn-outline-primary m-1" title="Fiche"><i class="fas fa-chart-bar"></i></a>
                                </div>
                                <div class="d-flex">
                                    <button type="button" data-toggle="modal" data-target="#deleteModal" data-text="{{ $vegetal->name }}" data-href="{{ route('deleteVegetal', ['id' => $vegetal->id]) }}" class="btn btn-circle btn-outline-danger m-1" title="Supprimer"><i class="fas fa-trash"></i></button>
                                    <button type="button" data-toggle="modal" data-target="#addSizeModal" data-vegetal="{{ $vegetal->id }}" class="btn btn-circle btn-outline-dark m-1" title="Nouvelle taille"><i class="fas fa-ruler"></i></button>
                                </div>
                                <div class="d-flex">
                                    <form method="post" action="{{ route('storeWater', ['id' => $vegetal->id]) }}">
                                        @csrf

                                        <button type="submit" class="btn btn-circle btn-outline-info m-1" title="Arroser"><i class="fas fa-tint"></i></button>
                                    </form>
                                    <form method="post" action="{{ route('storeFertilizer', ['id' => $vegetal->id]) }}">
                                        @csrf

                                        <button type="submit" class="btn btn-circle btn-outline-success m-1" title="Ajouter de l'engrais"><i class="fas fa-arrow-up"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="card-footer p-0">
                        @if (!empty($vegetal->arrosee))
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped bg-info" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" id="pgbar-{{ $vegetal->id }}"></div>
                            </div>
                        @endif
                    </div> --}}
                </div>
            </div>
        @endforeach
    </div>

    <div class="d-flex justify-content-end">
        <button type="button" data-toggle="modal" data-target="#addVegetalModal" class="btn btn-primary btnAction"><i class="fas fa-plus pr-2"></i>Ajouter une plante</button>
    </div>

    <div class="modal fade" id="addVegetalModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="aVegetalModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form method="post" action="{{ route('storeVegetal') }}">
                @csrf
                @method('post')

                <div class="modal-content">
                    <div class="modal-header py-2">
                        <h5 class="modal-title" id="aVegetalModal">Ajouter une plante</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group required">
                            <label>Nom</label>
                            <input type="text" class="form-control" name="name" required>
                        </div>
                        <div class="form-group required">
                            <label>Espèce</label>
                            <select class="form-control" name="espece_id" required>
                                @foreach ($especes as $espece)
                                    <option value="{{ $espece->id }}">{{ $espece->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Date de plantation</label>
                            <input class="form-control" type="date" name="date_plantation">
                        </div>
                    </div>
                    <div class="modal-footer py-1">
                        <button type="submit" class="btn btn-circle btn-outline-success"><i class="fas fa-check"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
