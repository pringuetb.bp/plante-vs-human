@extends('layouts.app')

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="mb-0 text-gray-800">Profil</h1>
    </div>

    <form method="post" action="{{ route('updateUser', Auth::user()->id) }}">
        @csrf
        @method('put')

        <div class="row row-cols-1 row-cols-lg-2">

            <div class="col">
                <div class="pb-4">
                    <div class="card shadow h-100">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="editAction">
                                        <div class="text-xs font-weight-bold text-uppercase mb-1">Nom</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ Auth::user()->name }}</div>
                                    </div>
                                    <div class="form-group required mb-0 editAction d-none">
                                        <label class="text-xs font-weight-bold text-uppercase mb-1">Nom</label>
                                        <input type="text" class="form-control" name="name" value="{{ old('name') ? old('name') : Auth::user()->name }}" minlength="3" required>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-user fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pb-4">
                    <div class="card shadow h-100">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="editAction">
                                        <div class="text-xs font-weight-bold text-uppercase mb-1">Email</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800 editAction">{{ Auth::user()->email }}</div>
                                    </div>
                                    <div class="form-group required mb-0 editAction d-none">
                                        <label class="text-xs font-weight-bold text-uppercase mb-1">Email</label>
                                        <input type="email" class="form-control editAction d-none" name="email" value="{{ old('email') ? old('email') : Auth::user()->email }}" minlength="3" required>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-envelope fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col pb-4">
                <div class="card shadow h-100">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-uppercase mb-1">Mot de passe</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800 editAction">******</div>
                                <div class="editAction d-none">
                                    <div class="form-group required mb-1">
                                        <label class="mb-1">Mot de passe</label>
                                        <input type="password" class="form-control" name="password" minlength="8" required>
                                    </div>
                                    <div class="form-group mb-1">
                                        <label class="mb-1">Nouveau mot de passe</label>
                                        <input type="password" class="form-control" name="new_password" minlength="8">
                                    </div>
                                    <div class="form-group mb-1">
                                        <label class="mb-1">Confirmer le nouveau mot de passe</label>
                                        <input type="password" class="form-control" name="new_password_confirmation" minlength="8">
                                    </div>
                                </div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-key fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="d-flex justify-content-end">
            <button type="submit" class="btn btn-circle btn-outline-success mr-2 editAction d-none"><i class="fas fa-save"></i></button>
            <button type="button" onclick="editAction()" class="btn btn-circle btn-outline-danger editAction d-none"><i class="fas fa-times"></i></button>
            <button type="button" onclick="editAction()" class="btn btn-circle btn-outline-warning mr-2 btnAction"><i class="fas fa-edit"></i></button>
            <button type="button" data-toggle="modal" data-target="#validationModal" class="btn btn-circle btn-outline-danger editAction"><i class="fas fa-trash"></i></button>
        </div>

    </form>

    <div class="modal fade" id="validationModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="validModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body p-2">
                    <h5 class="modal-title text-center mb-2" id="validModal">Etes-vous sûr de vouloir supprimer votre compte ?</h5>
                    <div class="d-flex justify-content-between">
                        <a class="btn btn-circle btn-outline-danger" data-dismiss="modal"><i class="fas fa-times"></i></a>
                        <a class="btn btn-circle btn-outline-success pl-auto" href="{{ route('deleteUser', Auth::user()->id) }}"><i class="fas fa-check"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
